package cmdline

import (
	"fmt"
	"testing"
)

const (
	sampleCmdline = `kernel-1c:61:b4:21:97:68-start b2c.pipefail initrd=initrd1 initrd=initrd2 initrd=initrd3 b2c.poweroff_delay=15 b2c.run="-ti --tls-verify=false docker://ci-gateway:8002/mupuf/valve-infra/machine_registration:latest check" b2c.cache_device=auto,fstrim=never b2c.volume="myvol,mirror=gateway/job-1234,pull_on=pipeline_start|pipeline_end,push_on=changes,overwrite" b2c.filesystem="fs1,type=ext4,src=/dev/sda1,opts=old1|old2,opts=new1|new2"`
)

func TestSplitParameters(t *testing.T) {
	want := []string{
		`kernel-1c:61:b4:21:97:68-start`,
		`b2c.pipefail`,
		`initrd=initrd1`,
		`initrd=initrd2`,
		`initrd=initrd3`,
		`b2c.poweroff_delay=15`,
		`b2c.run="-ti --tls-verify=false docker://ci-gateway:8002/mupuf/valve-infra/machine_registration:latest check"`,
		`b2c.cache_device=auto,fstrim=never`,
		`b2c.volume="myvol,mirror=gateway/job-1234,pull_on=pipeline_start|pipeline_end,push_on=changes,overwrite"`,
		`b2c.filesystem="fs1,type=ext4,src=/dev/sda1,opts=old1|old2,opts=new1|new2"`,
	}

	for i, param := range splitParameters(sampleCmdline) {
		if param != want[i] {
			t.Fatalf("The parameter ID #%d is unexpected. Expected '%s' but got '%s'", i, want[i], param)
		}
	}
}

func compareStringField(t *testing.T, got string, want string, field string) {
	if got != want {
		t.Fatalf("Field %s mismatch: Got '%s' but wanted '%s'", field, got, want)
	}
}

func compareRuneField(t *testing.T, got rune, want rune, field string) {
	if got != want {
		t.Fatalf("Field %s mismatch: Got '%c' but wanted '%c'", field, got, want)
	}
}

func compareStringSlice(t *testing.T, got []string, want []string, fieldName string) {
	if len(got) != len(want) {
		t.Fatalf("Field %s mismatch: Expected %d elements but got %d", fieldName, len(want), len(got))
	}

	for i, val := range got {
		if val != want[i] {
			t.Fatalf("\nExpected: %q\nGot: %q\nThe argument #%d differs: Expected '%s' but got '%s'", want, got, i, val, want[i])
		}
	}
}

func checkExpectation(t *testing.T, optionQuery OptionQuery, want Option) *Option {
	opt := FindOptionInString(sampleCmdline, optionQuery)

	compareStringField(t, opt.OptionQuery.Name, want.OptionQuery.Name, "OptionQuery.Name")
	compareRuneField(t, opt.OptionQuery.FieldSeparator, want.OptionQuery.FieldSeparator, "OptionQuery.FieldSeparator")
	compareRuneField(t, opt.OptionQuery.ListSeparator, want.OptionQuery.ListSeparator, "OptionQuery.ListSeparator")

	if len(want.Values) != len(opt.Values) {
		t.Fatalf("Expected %d instances of the parameter but got %d", len(want.Values), len(opt.Values))
	}

	for i, value := range opt.Values {
		want_value := want.Values[i]

		compareStringField(t, value.RawValue, want_value.RawValue, "RawValue")

		if len(value.Args) != len(want_value.Args) {
			t.Fatalf("Did not get the same amount of arguments: Expected %d but got %d (%q)",
				len(want_value.Args), len(value.Args), value.Args)
		}
		for i, arg := range value.Args {
			if arg != want_value.Args[i] {
				t.Fatalf("\nExpected: %q\nGot: %q\nThe argument #%d differs: Expected '%s' but got '%s'", want_value.Args, value.Args, i, want_value.Args[i], arg)
			}
		}
	}

	return opt
}

func TestFindOptionInString__missing_value(t *testing.T) {
	q := OptionQuery{Name: "missing", FieldSeparator: ',', ListSeparator: '|'}
	opt := checkExpectation(t, q, Option{OptionQuery: q})

	if opt.Value() != nil {
		t.Fatalf("Value() unexpectedly returned a value: Got %s", opt.Value())
	}
}

func TestFindOptionInString__weird_name(t *testing.T) {
	q := OptionQuery{Name: "kernel-1c:61:b4:21:97:68-start"}
	want := Option{OptionQuery: OptionQuery{Name: "kernel-1c:61:b4:21:97:68-start", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: "", Args: []string{}, KwArgs: map[string][]string{}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__repeated_parameter(t *testing.T) {
	q := OptionQuery{Name: "initrd"}
	want := Option{OptionQuery: OptionQuery{Name: "initrd", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: "initrd1", Args: []string{"initrd1"}},
			{RawValue: "initrd2", Args: []string{"initrd2"}},
			{RawValue: "initrd3", Args: []string{"initrd3"}}}}
	opt := checkExpectation(t, q, want)

	// Check that the returned value is always the last one
	value_want := "initrd3"
	if opt.Value().RawValue != value_want {
		t.Fatalf("Value() returned the wrong value: Expected %s but got %s", value_want, opt.Value().RawValue)
	}

	missingField := opt.Value().KwArg("missing", "default")
	if missingField != "default" {
		t.Fatalf("KwArg() of a missing field did not yield the expected default: Expected 'default' but got '%s'", missingField)
	}

	missingListField := opt.Value().KwArgAsList("missing")
	if len(missingListField) > 0 {
		t.Fatalf("KwArgAsList() of a missing field returned a non-empty list: Got '%q'", missingListField)
	}
}

func TestFindOptionInString__simple_parameter(t *testing.T) {
	q := OptionQuery{Name: "b2c.pipefail"}
	want := Option{OptionQuery: OptionQuery{Name: "b2c.pipefail", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: "", Args: []string{}, KwArgs: map[string][]string{}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__parameter_with_argument(t *testing.T) {
	q := OptionQuery{Name: "b2c.poweroff_delay"}
	want := Option{OptionQuery: OptionQuery{Name: "b2c.poweroff_delay", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: "15", Args: []string{"15"}, KwArgs: map[string][]string{}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__parameter_with_quoted_argument(t *testing.T) {
	q := OptionQuery{Name: "b2c.run"}

	raw_val := "-ti --tls-verify=false docker://ci-gateway:8002/mupuf/valve-infra/machine_registration:latest check"
	want := Option{OptionQuery: OptionQuery{Name: "b2c.run", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: raw_val, Args: []string{raw_val}, KwArgs: map[string][]string{}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__parameter_with_arg_and_kwarg(t *testing.T) {
	q := OptionQuery{Name: "b2c.cache_device"}

	raw_val := "auto,fstrim=never"
	want := Option{OptionQuery: OptionQuery{Name: "b2c.cache_device", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: raw_val, Args: []string{"auto"},
			KwArgs: map[string][]string{"fstrim": {"never"}}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__parameter_with_mix(t *testing.T) {
	q := OptionQuery{Name: "b2c.volume"}

	raw_val := "myvol,mirror=gateway/job-1234,pull_on=pipeline_start|pipeline_end,push_on=changes,overwrite"
	want := Option{OptionQuery: OptionQuery{Name: "b2c.volume", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: raw_val, Args: []string{"myvol", "overwrite"},
			KwArgs: map[string][]string{"mirror": {"gateway/job-1234"},
				"pull_on": {"pipeline_start", "pipeline_end"}}}}}
	checkExpectation(t, q, want)
}

func TestFindOptionInString__overridden_kwarg(t *testing.T) {
	q := OptionQuery{Name: "b2c.filesystem"}

	raw_val := "fs1,type=ext4,src=/dev/sda1,opts=old1|old2,opts=new1|new2"
	want := Option{OptionQuery: OptionQuery{Name: "b2c.filesystem", FieldSeparator: ',', ListSeparator: '|'},
		Values: []OptionValue{{RawValue: raw_val, Args: []string{"fs1"},
			KwArgs: map[string][]string{"type": {"ext4"},
				"src":  {"/dev/sda1"},
				"opts": {"new1", "new2"}}}}}
	opt := checkExpectation(t, q, want)
	compareStringSlice(t, opt.Value().KwArgAsList("opts"), []string{"new1", "new2"}, "opts")
	compareStringField(t, opt.Value().KwArg("src", "default"), "/dev/sda1", "src")

	// Check that trying to call KwArg() on a list generates a panic
	// No need to check whether `recover()` is nil. Just turn off the panic.
	defer func() { _ = recover() }()
	opt.Value().KwArg("opts", "")
	t.Errorf("did not panic")
}

func readFileMockFail(filepath string) ([]byte, error) {
	return nil, fmt.Errorf("File doesn't exist")
}

func TestFindOption__readFileFailure(t *testing.T) {
	readFile = readFileMockFail

	opt, err := FindOption(OptionQuery{Name: "b2c.volume"})
	if err == nil {
		t.Fatalf("No errors raised, despite failing to read the file")
	}

	if err.Error() != "File doesn't exist" {
		t.Fatalf("Unexpected error message '%s'", err.Error())
	}

	if opt != nil {
		t.Fatalf("Got a value when none were expected")
	}
}

func readFileMock(filepath string) ([]byte, error) {
	return []byte(sampleCmdline), nil
}

func TestFindOption__success(t *testing.T) {
	readFile = readFileMock

	opt, err := FindOption(OptionQuery{Name: "b2c.poweroff_delay"})

	if err != nil {
		t.Fatalf("An unexpected error was raised: %s", err.Error())
	}

	if len(opt.Values) != 1 {
		t.Fatalf("Unexpected amount of results found: Expected 1 but got %d", len(opt.Values))
	}

	if opt.Values[0].RawValue != "15" {
		t.Fatalf("Did not get the expected return value")
	}
}
