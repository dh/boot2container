package cache_device

import (
	"fmt"
	"testing"
	"time"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"

	"github.com/diskfs/go-diskfs/partition"
	"github.com/diskfs/go-diskfs/partition/gpt"
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

func compareStringSlice(t *testing.T, fieldName string, got []string, want []string) {
	if len(got) != len(want) {
		t.Fatalf("%s mismatch: Expected %d elements but got %d\nExpected: %q\nGot: %q", fieldName, len(want), len(got), want, got)
	}

	for i, val := range got {
		if val != want[i] {
			t.Fatalf("\nExpected: %q\nGot: %q\nThe argument #%d differs: Expected '%s' but got '%s'", want, got, i, val, want[i])
		}
	}
}

// -------------------------------- BlockDev ----------------------------------

var (
	blkDisk = BlockDev{_name: "sda", _path: "/dev/sda", _type: Disk, _size: CACHE_DEVICE_MIN_SIZE_MiB * 1024 * 1024,
		_fsType: CACHE_PARTITION_FS, _fsLabel: CACHE_PARTITION_LABEL}
)

func TestBlockDevType(t *testing.T) {
	assertEqual(t, "String", Disk.String(), "Disk")
	assertEqual(t, "String", Partition.String(), "Partition")
}

func TestBlockDevIsSuitableAsCachePartition__SuitableDrive(t *testing.T) {
	isSuitable, issues := blkDisk.IsSuitableAsCachePartition()

	compareStringSlice(t, "Issues", issues, []string{})
	assertEqual(t, "isSuitable", isSuitable, true)
}

func TestBlockDevIsSuitableAsCachePartition__EmptyPartitionTooSmall(t *testing.T) {
	blk := BlockDev{_name: "sda", _path: "/dev/sda", _type: Partition, _size: CACHE_DEVICE_MIN_SIZE_MiB*1024*1024 - 1,
		_fsType: "vfat", _fsLabel: CACHE_PARTITION_LABEL}

	isSuitable, issues := blk.IsSuitableAsCachePartition()

	compareStringSlice(t, "Issues", issues,
		[]string{"The file system is not EXT4",
			fmt.Sprintf("The size is under %d MiB", CACHE_DEVICE_MIN_SIZE_MiB)})
	assertEqual(t, "isSuitable", isSuitable, false)
}

func TestBlockDevSuitabilityScore(t *testing.T) {
	assertEqual(t, "Score nvme0n1", BlockDev{_path: "/dev/nvme0n1", _size: 100}.SuitabilityScore(), float32(500))
	assertEqual(t, "Score vdc", BlockDev{_path: "/dev/vdc", _size: 100}.SuitabilityScore(), float32(100))
	assertEqual(t, "Score sdb", BlockDev{_path: "/dev/sdb", _size: 100}.SuitabilityScore(), float32(100))
	assertEqual(t, "Score mmcblk2", BlockDev{_path: "/dev/mmcblk2", _size: 100}.SuitabilityScore(), float32(50))
	assertEqual(t, "Score hdc", BlockDev{_path: "/dev/hdc", _size: 100}.SuitabilityScore(), float32(50))
}

func TestBlockDevToFilesystem(t *testing.T) {
	name := "NaMe"
	want := filesystem.FilesystemConfig{Name: name, Src: blkDisk.Path(), Type: blkDisk.FSType()}
	gotIf := blkDisk.ToFilesystem(name)

	got, _ := gotIf.(filesystem.FilesystemConfig)
	if !got.Equal(want) {
		t.Fatalf("The filesystems did not watch:\nExpected: %s\n     Got: %s", want.String(), got.String())
	}
}

// ------------------------------- BlockDevices -------------------------------

var (
	MiB     uint64 = 1024 * 1024
	blkDevs        = BlockDevices{
		BlockDev{_name: "sda", _path: "/dev/sda", _type: Disk, _size: 1024 * MiB},
		BlockDev{_name: "sda1", _path: "/dev/sda1", _type: Partition, _size: 128 * MiB, _fsType: "vfat", _fsLabel: "ESP"},
		BlockDev{_name: "sda2", _path: "/dev/sda2", _type: Partition, _size: 894 * MiB, _fsType: "ext4", _fsLabel: "B2C_CACHE"},
		BlockDev{_name: "nvme0n1", _path: "/dev/nvme0n1", _type: Disk, _size: 2048 * MiB},
		BlockDev{_name: "nvme0n1p1", _path: "/dev/nvme0n1p1", _type: Partition, _size: 128 * MiB, _fsType: "vfat", _fsLabel: "ESP"},
		BlockDev{_name: "nvme0n1p2", _path: "/dev/nvme0n1p2", _type: Partition, _size: 1918 * MiB, _fsType: "ext4", _fsLabel: "B2C_CACHE"},
		BlockDev{_name: "sdb", _path: "/dev/sdb", _type: Disk, _size: 1536 * MiB},
		BlockDev{_name: "sdb1", _path: "/dev/sdb1", _type: Partition, _size: 128 * MiB, _fsType: "vfat", _fsLabel: "ESP"},
		BlockDev{_name: "sdb2", _path: "/dev/sdb2", _type: Partition, _size: 1408 * MiB, _fsType: "ext4", _fsLabel: "B2C_CACHE"},
	}
)

func TestBlockDevicesFilterFSLabel(t *testing.T) {
	devs := blkDevs.FilterFSLabel("ESP")
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	for _, b := range devs {
		if b.FSLabel() != "ESP" {
			t.Fatalf("FilterFSLabel returned a block device with the label '%s'", b.FSLabel())
		}
	}
}

func TestBlockDevicesFilterFSType(t *testing.T) {
	devs := blkDevs.FilterFSType("ext4")
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	for _, b := range devs {
		if b.FSType() != "ext4" {
			t.Fatalf("FilterFSType returned a block device with the '%s' filesystem", b.FSType())
		}
	}
}

func TestBlockDevicesFilterType(t *testing.T) {
	devs := blkDevs.FilterType(Disk)
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	for _, b := range devs {
		if b.Type() != Disk {
			t.Fatalf("FilterType returned a block device with the type '%s'", b.Type().String())
		}
	}
}

func TestBlockDevicesFilterSize__NoMinimumSize(t *testing.T) {
	devs := blkDevs.FilterSize(0, 256)
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	for _, b := range devs {
		if b.Size() > 256*MiB {
			t.Fatalf("FilterSize returned a block device with the size %d", b.Size())
		}
	}
}

func TestBlockDevicesFilterSize__NoMaximumSize(t *testing.T) {
	devs := blkDevs.FilterSize(256, 0)
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	for _, b := range devs {
		if b.Size() < 256*MiB {
			t.Fatalf("FilterSize returned a block device with the size %d", b.Size())
		}
	}
}

func TestBlockDevicesSortBySuitability(t *testing.T) {
	devs := blkDevs.FilterSize(0, 0)
	devs.SortBySuitability()
	if len(devs) == 0 {
		t.Fatalf("No block devices found")
	}

	curScore := devs[0].SuitabilityScore()
	for _, b := range devs {
		if b.SuitabilityScore() < curScore {
			devs.PrintList("not-so-ordered block devices")
			t.Fatalf("The devices were not ordered by descending score")
		} else {
			curScore = b.SuitabilityScore()
		}
	}
}

func TestBlockDevicesPrintList(t *testing.T) {
	devs := blkDevs.FilterFSLabel("ESP")
	want := `Found the following ESP partitions (3):
 - sda1 (Partition): size=0.13 GB, type=vfat, label=ESP, score=134217728
 - nvme0n1p1 (Partition): size=0.13 GB, type=vfat, label=ESP, score=671088640
 - sdb1 (Partition): size=0.13 GB, type=vfat, label=ESP, score=134217728
`
	assertEqual(t, "PrintListFull", want, devs.PrintList("ESP partitions"))

	blkName := "Custom Devices"
	want = fmt.Sprintf("No %s found\n", blkName)
	assertEqual(t, "PrintListEmpty", want, BlockDevices{}.PrintList(blkName))
}

// ---------------------------- CacheDeviceConfig -----------------------------

const (
	MOUNTPOINT_WANT = "/storage"
	formatLabelWant = "B2C_CACHE"
)

type FilesystemConfigMock struct {
	src string

	mountCalled bool
	mount       func(mountPoint string) error

	formatCalled bool
	format       func(label string, features []string) error

	fstrimCalled bool
	fstrim       func(mountPoint string) error
}

func (fs FilesystemConfigMock) Source() string {
	return fs.src
}

func (fs FilesystemConfigMock) Mount(mountPoint string) error {
	return fs.mount(mountPoint)
}

func (fs FilesystemConfigMock) Format(label string, features []string) error {
	return fs.format(label, features)
}

func (fs FilesystemConfigMock) Fstrim(mountPoint string) error {
	return fs.fstrim(mountPoint)
}

func defaultFilesystem() *FilesystemConfigMock {
	fs := &FilesystemConfigMock{src: "/dev/sda1"}

	fs.mount = func(mountPoint string) error {
		fs.mountCalled = true
		if mountPoint == MOUNTPOINT_WANT {
			return nil
		} else {
			return fmt.Errorf("Unexpected mount point: Expected %s but got %s.", MOUNTPOINT_WANT, mountPoint)
		}
	}

	fs.format = func(label string, features []string) error {
		fs.formatCalled = true
		return nil
	}

	fs.fstrim = func(mountPoint string) error {
		fs.fstrimCalled = true
		if mountPoint == MOUNTPOINT_WANT {
			return nil
		} else {
			return fmt.Errorf("Unexpected mount point: Expected %s but got %s.", MOUNTPOINT_WANT, mountPoint)
		}
	}

	return fs
}

func TestCacheDeviceConfigMountFilesystem__SuccessWithFstrimPipelineStart(t *testing.T) {
	cfg := CacheDeviceConfig{Fstrim: "pipeline_start"}

	fs := defaultFilesystem()
	if !cfg.mountFilesystem(fs, MOUNTPOINT_WANT) {
		t.Fatalf("Did not expect mountFilesystem to return false")
	}

	assertEqual(t, "mountCalled", fs.mountCalled, true)
	assertEqual(t, "fstrimCalled", fs.fstrimCalled, true)
	assertEqual(t, "formatCalled", fs.formatCalled, false)
}

func TestCacheDeviceConfigMountFilesystem__Fail(t *testing.T) {
	cfg := CacheDeviceConfig{Fstrim: "pipeline_start"}

	fs := defaultFilesystem()
	fs.mount = func(mountPoint string) error { fs.mountCalled = true; return fmt.Errorf("OOOOOPSSSSS") }

	if cfg.mountFilesystem(fs, MOUNTPOINT_WANT) {
		t.Fatalf("Did not expect mountFilesystem to return false")
	}

	assertEqual(t, "mountCalled", fs.mountCalled, true)
	assertEqual(t, "fstrimCalled", fs.fstrimCalled, false)
	assertEqual(t, "formatCalled", fs.formatCalled, false)
}

func TestCacheDeviceConfigMountFilesystem__InvalidFstrim(t *testing.T) {
	cfg := CacheDeviceConfig{Fstrim: "invalid"}

	fs := defaultFilesystem()

	// Verify that we panic when presented with an unknown fstrim mode
	defer func() {
		_ = recover()

		assertEqual(t, "mountCalled", fs.mountCalled, true)
		assertEqual(t, "fstrimCalled", fs.fstrimCalled, false)
	}()

	cfg.mountFilesystem(fs, MOUNTPOINT_WANT)
	t.Errorf("did not panic")
}

func testCacheDeviceConfUseSpecifiedBlockDevice(t *testing.T, roundsBeforeSuccess int, fs *FilesystemConfigMock,
	b *BlockDev) bool {
	sleepCallCount := 0
	Sleep = func(d time.Duration) {
		assertEqual(t, "Sleep duration", d, DEVICE_POLLING_DELAY)
		sleepCallCount += 1
	}

	createBlockDeviceCallCount := 0
	CreateBlockDevice = func(path string) (*BlockDev, error) {
		assertEqual(t, "createBlockDevice path", path, "/dev/sda1")
		createBlockDeviceCallCount += 1

		if createBlockDeviceCallCount < roundsBeforeSuccess {
			return nil, fmt.Errorf("Missing")
		} else {
			return b, nil
		}
	}

	cfg := CacheDeviceConfig{Fstrim: "never", Filesystem: fs}
	ret := cfg.useSpecifiedBlockDevice(MOUNTPOINT_WANT)

	assertEqual(t, "createBlockDeviceCallCount", createBlockDeviceCallCount, roundsBeforeSuccess)

	minSleepCallCount := createBlockDeviceCallCount - 1
	if sleepCallCount < minSleepCallCount {
		t.Fatalf("Expected at least %d calls to Sleep(), got %d", minSleepCallCount, sleepCallCount)
	}

	return ret
}

func TestCacheDeviceConfUseSpecifiedBlockDevice__DeviceMissing(t *testing.T) {
	wantLoopCount := int(DEVICE_POLLING_TIMEOUT / DEVICE_POLLING_DELAY)
	fs := defaultFilesystem()
	ret := testCacheDeviceConfUseSpecifiedBlockDevice(t, wantLoopCount, fs, nil)

	assertEqual(t, "Return value", ret, false)
	assertEqual(t, "mountCalled", fs.mountCalled, false)
	assertEqual(t, "formatCalled", fs.formatCalled, false)
}

func TestCacheDeviceConfUseSpecifiedBlockDevice__SuitableCachePartition(t *testing.T) {
	wantLoopCount := 2
	fs := defaultFilesystem()
	b := &BlockDev{_fsType: CACHE_PARTITION_FS, _size: CACHE_DEVICE_MIN_SIZE_MiB * 1024 * 1024}

	ret := testCacheDeviceConfUseSpecifiedBlockDevice(t, wantLoopCount, fs, b)

	assertEqual(t, "Return value", ret, true)
	assertEqual(t, "mountCalled", fs.mountCalled, true)
	assertEqual(t, "formatCalled", fs.formatCalled, false)
}

func TestCacheDeviceConfUseSpecifiedBlockDevice__UnsuitableCachePartition(t *testing.T) {
	wantLoopCount := 2
	fs := defaultFilesystem()
	b := &BlockDev{_fsType: "invalidFS", _size: CACHE_DEVICE_MIN_SIZE_MiB * 1024 * 1024}

	ret := testCacheDeviceConfUseSpecifiedBlockDevice(t, wantLoopCount, fs, b)

	assertEqual(t, "Return value", ret, true)
	assertEqual(t, "formatCalled", fs.formatCalled, true)
	assertEqual(t, "mountCalled", fs.mountCalled, true)
}

func TestCacheDeviceConfUseSpecifiedBlockDevice__UnsuitableCachePartitionWithFormatFailure(t *testing.T) {
	wantLoopCount := 2
	fs := defaultFilesystem()
	b := &BlockDev{_fsType: "invalidFS", _size: CACHE_DEVICE_MIN_SIZE_MiB * 1024 * 1024}

	fs.format = func(label string, features []string) error {
		fs.formatCalled = true
		return fmt.Errorf("Failed to format")
	}

	ret := testCacheDeviceConfUseSpecifiedBlockDevice(t, wantLoopCount, fs, b)

	assertEqual(t, "Return value", ret, false)
	assertEqual(t, "formatCalled", fs.formatCalled, true)
	assertEqual(t, "mountCalled", fs.mountCalled, false)
}

func TestCacheDeviceConfFindMostSuitableExistingCachePartition__Success(t *testing.T) {
	cfg := CacheDeviceConfig{}
	blk := cfg.findMostSuitableExistingCachePartition(blkDevs)
	assertEqual(t, "blk.Path()", blk.Path(), "/dev/nvme0n1p2")
}

func TestCacheDeviceConfFindMostSuitableExistingCachePartition__NoneFound(t *testing.T) {
	cfg := CacheDeviceConfig{}

	customBlkDevs := BlockDevices{
		&BlockDev{_name: "sda", _path: "/dev/sda", _type: Disk, _size: 1024 * MiB},
		&BlockDev{_name: "sda1", _path: "/dev/sda1", _type: Partition, _size: 128 * MiB, _fsType: "vfat", _fsLabel: "ESP"},
	}

	blk := cfg.findMostSuitableExistingCachePartition(customBlkDevs)
	assertEqual(t, "Block Device", blk, nil)
}

func TestCacheDeviceConfFindMostSuitableCacheDisk__Success(t *testing.T) {
	cfg := CacheDeviceConfig{}
	blkDev := cfg.findMostSuitableCacheDisk(blkDevs)

	assertEqual(t, "blkDev.Path", blkDev.Path(), "/dev/nvme0n1")
}

func TestCacheDeviceConfFindMostSuitableCacheDisk__NoneFound(t *testing.T) {
	cfg := CacheDeviceConfig{}
	devs := BlockDevices{&BlockDev{_fsType: CACHE_PARTITION_FS, _size: CACHE_DEVICE_MIN_SIZE_MiB*1024*1024 - 1}}

	if blkDev := cfg.findMostSuitableCacheDisk(devs); blkDev != nil {
		t.Errorf("Unexpectedly found a suitable cache device: %v", blkDev)
	}
}

func TestCacheDeviceConfResetDiskPartitionTable__Success(t *testing.T) {
	cfg := CacheDeviceConfig{}
	blkDev := &BlockDev{_fsType: CACHE_PARTITION_FS, _size: CACHE_DEVICE_MIN_SIZE_MiB * 1024 * 1024}

	table := cfg.resetDiskPartitionTable(blkDev)

	assertEqual(t, "table.LogicalSectorSize", table.LogicalSectorSize, 512)
	assertEqual(t, "table.PhysicalSectorSize", table.PhysicalSectorSize, 512)
	assertEqual(t, "table.ProtectiveMBR", table.ProtectiveMBR, true)
	assertEqual(t, "len(table.Partitions)", len(table.Partitions), 2)

	// Test ESP partition
	assertEqual(t, "table.Partitions[0].Start", table.Partitions[0].Start, uint64(2048))
	assertEqual(t, "table.Partitions[0].End", table.Partitions[0].End, uint64(264191))
	assertEqual(t, "table.Partitions[0].Type", table.Partitions[0].Type, gpt.EFISystemPartition)
	assertEqual(t, "table.Partitions[0].Name", table.Partitions[0].Name, "EFI System")

	// Test cache partition
	assertEqual(t, "table.Partitions[1].Start", table.Partitions[1].Start, uint64(264191))
	assertEqual(t, "table.Partitions[1].End", table.Partitions[1].End, uint64(524287))
	assertEqual(t, "table.Partitions[1].Type", table.Partitions[1].Type, gpt.LinuxFilesystem)
	assertEqual(t, "table.Partitions[1].Name", table.Partitions[1].Name, CACHE_PARTITION_LABEL)
}

func TestCacheDeviceConfResetDiskPartitionTable__DiskTooSmall(t *testing.T) {
	cfg := CacheDeviceConfig{}
	blkDev := &BlockDev{_fsType: CACHE_PARTITION_FS, _size: CACHE_DEVICE_MIN_SIZE_MiB*1024*1024 - 1}

	// Verify that we panic when the cache device is too small
	defer func() {
		_ = recover()
	}()
	cfg.resetDiskPartitionTable(blkDev)
	t.Errorf("did not panic")
}

type BlockDevMock struct {
	_name string
	_path string
	_type BlockDevType
	_size uint64
	// TODO: add the transport (NVME/USB/MMC/...)

	// Only applicable when Type=Partition
	_fsType  string
	_fsLabel string

	_fs FilesystemLike

	isSuitableAsCachePartition func() (suitable bool, issues []string)
	_score                     float32
	partitionById              func(int) BlockDevLike
}

func (b BlockDevMock) Name() string {
	return b._name
}

func (b BlockDevMock) Path() string {
	return b._path
}

func (b BlockDevMock) Type() BlockDevType {
	return b._type
}

func (b BlockDevMock) Size() uint64 {
	return b._size
}

func (b BlockDevMock) FSType() string {
	return b._fsType
}

func (b BlockDevMock) FSLabel() string {
	return b._fsLabel
}

func (b BlockDevMock) IsSuitableAsCachePartition() (suitable bool, issues []string) {
	if b.isSuitableAsCachePartition != nil {
		return b.isSuitableAsCachePartition()
	} else {
		suitable = true
		issues = []string{}
		return
	}
}

func (b BlockDevMock) SuitabilityScore() float32 {
	return b._score
}

func (b BlockDevMock) ToFilesystem(name string) FilesystemLike {
	return b._fs
}

func (b BlockDevMock) PartitionById(id int) BlockDevLike {
	if b.partitionById != nil {
		return b.partitionById(id)
	} else {
		path := fmt.Sprintf("%s-%d", b._path, id)
		return BlockDevMock{_name: "TestBlkPart", _path: path, _type: Partition, _size: 128 * MiB, _score: 1}
	}
}

// ---------------------------- Cache device conf -----------------------------

func TestCacheDeviceConfResetDiskAndMount_NoSuitableDevice(t *testing.T) {
	cfg := CacheDeviceConfig{}
	ret := cfg.resetDiskAndMount(BlockDevices{}, "/storage")
	assertEqual(t, "Return value", ret, false)
}

func testCacheDeviceConfResetDiskAndMount(t *testing.T, partitioningSuccess bool, partitionFound bool, formatSuccess bool) {
	partitionByIdCalled := false
	partitionById := func(id int) BlockDevLike {
		partitionByIdCalled = true
		if partitionFound {
			path := fmt.Sprintf("/dev/sda%d", id)
			return BlockDevMock{_name: "TestBlkPart", _path: path, _type: Partition, _size: 128 * MiB, _score: 1}
		} else {
			return nil
		}
	}

	blkDevs := BlockDevices{
		BlockDevMock{_name: "TestBlk", _path: "/dev/sda", _type: Disk, _size: 1024 * MiB, _score: 1000,
			partitionById: partitionById,
		},
	}

	partitionCacheDeviceCalled := false
	PartitionCacheDevice = func(_ BlockDevLike, _ partition.Table) bool {
		partitionCacheDeviceCalled = true
		return partitioningSuccess
	}

	formatCacheDeviceCalled := false
	fs := defaultFilesystem()
	FormatCacheDevice = func(b BlockDevLike) (FilesystemLike, error) {
		formatCacheDeviceCalled = true
		if formatSuccess {
			return fs, nil
		} else {
			return nil, fmt.Errorf("Format failed")
		}
	}

	cfg := CacheDeviceConfig{}
	ret := cfg.resetDiskAndMount(blkDevs, "/storage")

	expected_ret := partitioningSuccess && partitionFound && formatSuccess

	assertEqual(t, "Return value", ret, expected_ret)
	assertEqual(t, "partitionCacheDeviceCalled called", partitionCacheDeviceCalled, true)
	assertEqual(t, "partitionById called", partitionByIdCalled, partitioningSuccess)
	assertEqual(t, "formatCacheDeviceCalled called", formatCacheDeviceCalled, partitionFound)
	assertEqual(t, "Filesystem mount called", fs.mountCalled, expected_ret)
}

func TestCacheDeviceConfResetDiskAndMount_PartitioningFailed(t *testing.T) {
	testCacheDeviceConfResetDiskAndMount(t, false, false, false)
}

func TestCacheDeviceConfResetDiskAndMount_PartitionNotFound(t *testing.T) {
	testCacheDeviceConfResetDiskAndMount(t, true, false, false)
}

func TestCacheDeviceConfResetDiskAndMount_FormatFailed(t *testing.T) {
	testCacheDeviceConfResetDiskAndMount(t, true, true, false)
}

func TestCacheDeviceConfResetDiskAndMount_Success(t *testing.T) {
	testCacheDeviceConfResetDiskAndMount(t, true, true, true)
}

func TestCacheDeviceConfMount_None(t *testing.T) {
	cfg := CacheDeviceConfig{Mode: "none"}
	assertEqual(t, "Return value", cfg.Mount("/storage"), false)
}

func TestCacheDeviceConfMount_Filesystem(t *testing.T) {
	fs := defaultFilesystem()
	cfg := CacheDeviceConfig{Mode: "filesystem", Filesystem: fs}
	assertEqual(t, "Return value", cfg.Mount("/storage"), true)
	assertEqual(t, "Mount called", fs.mountCalled, true)
}

func TestCacheDeviceConfMount_Block(t *testing.T) {
	fs := defaultFilesystem()
	cfg := CacheDeviceConfig{Mode: "block", Filesystem: fs}
	assertEqual(t, "Return value", cfg.Mount("/storage"), true)
	assertEqual(t, "Mount called", fs.mountCalled, true)
}

func TestCacheDeviceConfMount_InvalidMode(t *testing.T) {
	cfg := CacheDeviceConfig{Mode: "invalid"}

	// Verify that we panic when using an invalid mode
	defer func() {
		_ = recover()
	}()
	cfg.Mount("/storage")
	t.Errorf("did not panic")
}

func TestCacheDeviceConfMount_AutoNoBlockDevices(t *testing.T) {
	GetBlockDevices = func() (BlockDevices, error) {
		return BlockDevices{}, nil
	}

	cfg := CacheDeviceConfig{Mode: "auto"}
	assertEqual(t, "Return value", cfg.Mount("/storage"), false)
}

func TestCacheDeviceConfMount_AutoExistingSuitableCachePartition(t *testing.T) {
	fs := defaultFilesystem()
	GetBlockDevices = func() (BlockDevices, error) {
		return BlockDevices{
			BlockDevMock{_name: "Block", _path: "/dev/blkmock", _type: Disk, _size: CACHE_DEVICE_MIN_SIZE_MiB * MiB},
			BlockDevMock{_name: "Partition1", _path: "/dev/blkmock1", _type: Partition,
				_size: CACHE_DEVICE_ESP_SIZE_MiB * MiB, _fsType: CACHE_PARTITION_FS,
				_fsLabel: CACHE_PARTITION_LABEL, _score: 1, _fs: fs},
		}, nil
	}

	cfg := CacheDeviceConfig{Mode: "auto"}
	assertEqual(t, "Return value", cfg.Mount("/storage"), true)
	assertEqual(t, "Format called", fs.formatCalled, false)
	assertEqual(t, "Mount called", fs.mountCalled, true)
}

func TestCacheDeviceConfMount_ResetExistingSuitableCachePartition(t *testing.T) {
	fs := defaultFilesystem()
	GetBlockDevices = func() (BlockDevices, error) {
		return BlockDevices{
			BlockDevMock{_name: "Block", _path: "/dev/blkmock", _type: Disk, _size: CACHE_DEVICE_MIN_SIZE_MiB * MiB},
			BlockDevMock{_name: "Partition1", _path: "/dev/blkmock1", _type: Partition,
				_size: CACHE_DEVICE_ESP_SIZE_MiB * MiB, _fsType: CACHE_PARTITION_FS,
				_fsLabel: CACHE_PARTITION_LABEL, _score: 1, _fs: fs},
		}, nil
	}

	cfg := CacheDeviceConfig{Mode: "reset"}
	assertEqual(t, "Return value", cfg.Mount("/storage"), true)
	assertEqual(t, "Format called", fs.formatCalled, true)
	assertEqual(t, "Mount called", fs.mountCalled, true)
}

func TestCacheDeviceConfMount_AutoWithNoExistingCachePartition(t *testing.T) {
	partitionByIdCalled := false
	partitionById := func(id int) BlockDevLike {
		partitionByIdCalled = true
		path := fmt.Sprintf("/dev/sda%d", id)
		return BlockDevMock{_name: "TestBlkPart", _path: path, _type: Partition, _size: 128 * MiB, _score: 1}
	}

	GetBlockDevices = func() (BlockDevices, error) {
		return BlockDevices{
			BlockDevMock{_name: "Block", _path: "/dev/blkmock", _type: Disk,
				_size: CACHE_DEVICE_MIN_SIZE_MiB * MiB, _score: 1,
				partitionById: partitionById,
			},
		}, nil
	}

	partitionCacheDeviceCalled := false
	PartitionCacheDevice = func(_ BlockDevLike, _ partition.Table) bool {
		partitionCacheDeviceCalled = true
		return true
	}

	formatCacheDeviceCalled := false
	fs := defaultFilesystem()
	FormatCacheDevice = func(b BlockDevLike) (FilesystemLike, error) {
		formatCacheDeviceCalled = true
		return fs, nil
	}

	cfg := CacheDeviceConfig{Mode: "auto"}
	assertEqual(t, "Return value", cfg.Mount("/storage"), true)
	assertEqual(t, "partitionByIdCalled", partitionByIdCalled, true)
	assertEqual(t, "partitionCacheDeviceCalled", partitionCacheDeviceCalled, true)
	assertEqual(t, "formatCacheDeviceCalled", formatCacheDeviceCalled, true)
}

// ----------------------------- Cmdline parsing ------------------------------

const (
	opt_name = `b2c.cache_device`

	cmdline_empty          = ``
	cmdline_none           = `b2c.cache_device=none`
	cmdline_auto           = `b2c.cache_device=auto`
	cmdline_path_to_device = `b2c.cache_device=/dev/sda`
	cmdline_filesystem     = `b2c.filesystem=myNFS_drive,type=nfs,src=10.0.0.1:/,opts=nodev|ro b2c.cache_device=myNFS_drive`

	cmdline_auto_with_fstrim = `b2c.cache_device=auto,fstrim=pipeline_start`

	cmdline_from_the_future = `b2c.cache_device=superauto,fstrim=continuous,newpositionalargument`
)

func checkExpectation(t *testing.T, parameters string, want CacheDeviceConfig) {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: opt_name})

	// Preload the filesystems
	fs_opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: "b2c.filesystem"})
	fses := filesystem.ListCmdlineFilesystems(fs_opt)

	cfg := ParseCmdline(opt, fses)

	if cfg.RawName != want.RawName {
		t.Fatalf("Unexpected name: Expected '%s' but got '%s'", want.RawName, cfg.RawName)
	} else if cfg.Mode != want.Mode {
		t.Fatalf("Unexpected mode: Expected '%s' but got '%s'", want.Mode, cfg.Mode)
	} else if cfg.Fstrim != want.Fstrim {
		t.Fatalf("Unexpected Fstrim mode: Expected '%s' but got '%s'", want.Fstrim, cfg.Fstrim)
	}
}

func TestParseCmdline__no_opt(t *testing.T) {
	var fses map[string]filesystem.FilesystemConfig
	cfg := ParseCmdline(nil, fses)

	want := CacheDeviceConfig{RawName: "", Mode: "none", Fstrim: "never"}
	if cfg.RawName != want.RawName {
		t.Fatalf("Unexpected name: Expected '%s' but got '%s'", want.RawName, cfg.RawName)
	} else if cfg.Mode != want.Mode {
		t.Fatalf("Unexpected mode: Expected '%s' but got '%s'", want.Mode, cfg.Mode)
	} else if cfg.Fstrim != want.Fstrim {
		t.Fatalf("Unexpected Fstrim mode: Expected '%s' but got '%s'", want.Fstrim, cfg.Fstrim)
	}
}

func TestParseCmdline__defaults(t *testing.T) {
	want := CacheDeviceConfig{RawName: "", Mode: "none", Fstrim: "never"}
	checkExpectation(t, cmdline_empty, want)
}

func TestParseCmdline__cmdline_none(t *testing.T) {
	want := CacheDeviceConfig{RawName: "none", Mode: "none", Fstrim: "never"}
	checkExpectation(t, cmdline_none, want)
}

func TestParseCmdline__cmdline_auto(t *testing.T) {
	want := CacheDeviceConfig{RawName: "auto", Mode: "auto", Fstrim: "never"}
	checkExpectation(t, cmdline_auto, want)
}

func TestParseCmdline__cmdline_path_to_device(t *testing.T) {
	want := CacheDeviceConfig{RawName: "/dev/sda", Mode: "block", Fstrim: "never"}
	checkExpectation(t, cmdline_path_to_device, want)
}

func TestParseCmdline__cmdline_filesystem(t *testing.T) {
	fs_want := filesystem.FilesystemConfig{Name: "myNFS_drive", Type: "nfs", Src: "10.0.0.1:/", Opts: []string{"ro", "nodev"}}

	want := CacheDeviceConfig{RawName: "myNFS_drive", Mode: "filesystem", Fstrim: "never", Filesystem: fs_want}
	checkExpectation(t, cmdline_filesystem, want)
}

func TestParseCmdline__cmdline_auto_with_fstrim(t *testing.T) {
	want := CacheDeviceConfig{RawName: "auto", Mode: "auto", Fstrim: "pipeline_start"}
	checkExpectation(t, cmdline_auto_with_fstrim, want)
}

func TestParseCmdline__cmdline_from_the_future(t *testing.T) {
	want := CacheDeviceConfig{RawName: "superauto", Mode: "none", Fstrim: "never"}
	checkExpectation(t, cmdline_from_the_future, want)
}
