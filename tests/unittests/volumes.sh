source ./tests/unittests/base.sh


# Mocked calls
MCLI_EXIT_CODE=0
mc() {
    local OLDIFS=$IFS IFS=' '
    mcli_calls="$mcli_calls$@\n"
    IFS=$OLDIFS
    return $MCLI_EXIT_CODE
}
mcli_calls=""


PODMAN_EXISTS_EXIT_CODE=0
PODMAN_INSPECT_EXIT_CODE=0
PODMAN_CREATE_EXIT_CODE=0
PODMAN_LIST_EXIT_CODE=0
PODMAN_LIST_RETURN_STR=""
podman() {
    local OLDIFS=$IFS IFS=' '
    local cur_call="$@"
    podman_calls="$podman_calls$@\n"
    IFS=$OLDIFS

    case $1 in
        volume)
            case $2 in
                create)
                    return $PODMAN_CREATE_EXIT_CODE
                    ;;
                inspect)
                    local path="/volume/local/path"
                    mkdir -p $path
                    echo $path
                    return $PODMAN_INSPECT_EXIT_CODE
                    ;;
                exists)
                    return $PODMAN_EXISTS_EXIT_CODE
                    ;;
                list)
                    # Make sure that the list call is done with the right parameters
                    if [ "$cur_call" != "volume list --noheading --format {{.Name}} --filter label=expiration=pipeline_end" ]; then
                        return 99
                    else
                        echo -e "$PODMAN_LIST_RETURN_STR"
                        return $PODMAN_LIST_EXIT_CODE
                    fi
                    ;;
            esac
            ;;
    esac
}
podman_calls=""


# Tests
testSetupVolumes() {
    test() {
        local SETUP_VOLUME_EXIT_CODE=0
        setup_volume() {
            local OLDIFS=$IFS IFS=' '
            setup_volume_calls="$setup_volume_calls$@\n"
            IFS=$OLDIFS
            return $SETUP_VOLUME_EXIT_CODE
        }

        ARG_MINIO="alias1,http://alias1_url,access1,secret1\nalias2,http://alias2_url,access2,secret2"
        ARG_VOLUME="volume1\nvolume2\n"

        start_subtest "Check that we can set up multiple volumes"
        mcli_calls=""
        setup_volume_calls=""
        setup_volumes
        assertEquals 0 $?
        assertEquals "--no-color alias set alias1 http://alias1_url access1 secret1\n--no-color alias set alias2 http://alias2_url access2 secret2\n" "$mcli_calls"
        assertEquals 'volume1\nvolume2\n' "$setup_volume_calls"

        start_subtest "Check that setup_volumes propagates errors coming from mcli"
        MCLI_EXIT_CODE=1
        mcli_calls=""
        setup_volume_calls=""
        setup_volumes
        assertEquals 1 $?
        assertEquals "--no-color alias set alias1 http://alias1_url access1 secret1\n" "$mcli_calls"
        assertEquals '' "$setup_volume_calls"

        start_subtest "Check that setup_volumes propagates errors in the volume-setup code"
        MCLI_EXIT_CODE=0
        SETUP_VOLUME_EXIT_CODE=1
        mcli_calls=""
        setup_volume_calls=""
        setup_volumes
        assertEquals 1 $?
        assertEquals "--no-color alias set alias1 http://alias1_url access1 secret1\n--no-color alias set alias2 http://alias2_url access2 secret2\n" "$mcli_calls"
        assertEquals 'volume1\n' "$setup_volume_calls"
    }

    run_unit_test test
}
suite_addTest testSetupVolumes


testSetupVolumeBasic() {
    test() {
        start_subtest "Check that a volume gets created if missing"
        podman_calls=""
        PODMAN_EXISTS_EXIT_CODE=1
        setup_volume "volume1"
        assertEquals $? 0
        assertEquals 'volume exists volume1\nvolume create --label expiration=never volume1\n' "$podman_calls"

        start_subtest "Check that setup_volume fails if a volume fails to get created"
        podman_calls=""
        PODMAN_EXISTS_EXIT_CODE=1
        PODMAN_CREATE_EXIT_CODE=1
        setup_volume "volume1"
        assertEquals 1 $?
        assertEquals 'volume exists volume1\nvolume create --label expiration=never volume1\n' "$podman_calls"

        start_subtest "Check that a volume is re-used if existing"
        podman_calls=""
        PODMAN_EXISTS_EXIT_CODE=0
        setup_volume "volume1"
        assertEquals 0 $?
        assertEquals 'volume exists volume1\n' "$podman_calls"
    }
    run_unit_test test
}
suite_addTest testSetupVolumeBasic


testSetupVolumeEncrypted() {
    test() {
        FSCRYPTCTL_GET_POLICY_EXIT_CODE=0
        FSCRYPTCTL_SET_POLICY_EXIT_CODE=0
        FSCRYPTCTL_ADD_KEY_EXIT_CODE=0
        fscryptctl() {
            local OLDIFS=$IFS IFS=' '
            fscryptctl_calls="$fscryptctl_calls$@\n"
            IFS=$OLDIFS

            case $1 in
                get_policy)
                    return $FSCRYPTCTL_GET_POLICY_EXIT_CODE
                    ;;
                set_policy)
                    return $FSCRYPTCTL_SET_POLICY_EXIT_CODE
                    ;;
                add_key)
                    echo "my_key_id"
                    return $FSCRYPTCTL_ADD_KEY_EXIT_CODE
                    ;;
            esac
        }
        fscryptctl_calls=""

        rm() {
            local OLDIFS=$IFS IFS=' '
            rm_calls="$rm_calls$@\n"
            IFS=$OLDIFS
        }
        rm_calls=""

        mkdir() {
            local OLDIFS=$IFS IFS=' '
            mkdir_calls="$mkdir_calls$@\n"
            IFS=$OLDIFS
        }
        mkdir_calls=""

        start_subtest "Basic use case"
        setup_volume "volume1,fscrypt_key=abcd"
        assertEquals 0 $?
        assertEquals 'get_policy /volume/local/path\nset_policy my_key_id /volume/local/path\nget_policy /volume/local/path\n' "$fscryptctl_calls"

        start_subtest "Kernel not supporting encryption"
        FSCRYPTCTL_ADD_KEY_EXIT_CODE=1
        fscryptctl_calls=""
        setup_volume "volume1,fscrypt_key=abcd"
        assertEquals 1 $?
        assertEquals 'get_policy /volume/local/path\n' "$fscryptctl_calls"
        FSCRYPTCTL_ADD_KEY_EXIT_CODE=0

        start_subtest "Kernel not supporting encryption / using the wrong key"
        FSCRYPTCTL_SET_POLICY_EXIT_CODE=1
        fscryptctl_calls=""
        rm_calls=""
        mkdir_calls=""
        setup_volume "volume1,fscrypt_key=abcd"
        assertEquals 1 $?
        assertEquals "" "$rm_calls"
        assertEquals "" "$mkdir_calls"
        assertEquals 'get_policy /volume/local/path\nset_policy my_key_id /volume/local/path\n' "$fscryptctl_calls"
        FSCRYPTCTL_SET_POLICY_EXIT_CODE=0

        start_subtest "Using the wrong key, but having fscrypt_reset_key"
        FSCRYPTCTL_SET_POLICY_EXIT_CODE=1
        fscryptctl_calls=""
        rm_calls=""
        mkdir_calls=""
        setup_volume "volume1,fscrypt_key=abcd,fscrypt_reset_key"
        assertEquals 1 $?  # We still fail because the second set_policy fails
        assertEquals "-rf /volume/local/path\n" "$rm_calls"
        assertEquals "/volume/local/path\n" "$mkdir_calls"
        assertEquals 'get_policy /volume/local/path\nset_policy my_key_id /volume/local/path\nset_policy my_key_id /volume/local/path\n' "$fscryptctl_calls"
        FSCRYPTCTL_SET_POLICY_EXIT_CODE=0
    }
    run_unit_test test
}
suite_addTest testSetupVolumeEncrypted


testSetupVolume__Mirror_with_push_pull_conditions() {
    test() {
        call_mcli_mirror_on_hook_conditions() {
            local OLDIFS=$IFS IFS=' '
            call_mcli_mirror_on_hook_conditions_calls="$call_mcli_mirror_on_hook_conditions_calls$@\n"
            IFS=$OLDIFS
        }

        # Simplified test
        start_subtest "Basic test"
        call_mcli_mirror_on_hook_conditions_calls=""
        setup_volume "volume,mirror=mirror_target,pull_on=pull_conditions,push_on=push_conditions"
        assertEquals "pull_conditions -r /volume/local/path -q --no-color  mirror_target .\npush_conditions -r /volume/local/path -q --no-color  . mirror_target\n" "$call_mcli_mirror_on_hook_conditions_calls"

        # Full test
        start_subtest "All options on"
        call_mcli_mirror_on_hook_conditions_calls=""
        setup_volume "volume,pull_from=pull_from,push_to=push_to,pull_on=pull_conditions,push_on=push_conditions,overwrite,remove,exclude=exclude,encrypt_key=mykey,preserve"
        assertEquals "pull_conditions -r /volume/local/path -q --no-color  --overwrite --remove --exclude exclude --encrypt-key mykey -a pull_from .\npush_conditions -r /volume/local/path -q --no-color  --overwrite --remove --exclude exclude --encrypt-key mykey -a . push_to\n" "$call_mcli_mirror_on_hook_conditions_calls"
    }

    run_unit_test test
}
suite_addTest testSetupVolume__Mirror_with_push_pull_conditions


testCall_mcli_mirror_on_hook_conditions() {
    test() {
        start_subtest "Test the 'pipeline_start' condition"
        B2C_HOOK_PIPELINE_START=""
        call_mcli_mirror_on_hook_conditions "pipeline_start" "args"
        call_mcli_mirror_on_hook_conditions "pipeline_start" "args2"
        assertEquals "mc mirror args\nmc mirror args2\n" "$B2C_HOOK_PIPELINE_START"

        start_subtest "Test the 'container_start' condition"
        B2C_HOOK_CONTAINER_START=""
        call_mcli_mirror_on_hook_conditions "container_start" "args"
        call_mcli_mirror_on_hook_conditions "container_start" "args2"
        assertEquals "mc mirror args\nmc mirror args2\n" "$B2C_HOOK_CONTAINER_START"

        start_subtest "Test the 'container_end' condition"
        B2C_HOOK_CONTAINER_END=""
        call_mcli_mirror_on_hook_conditions "container_end" "args"
        call_mcli_mirror_on_hook_conditions "container_end" "args2"
        assertEquals "mc mirror args\nmc mirror args2\n" "$B2C_HOOK_CONTAINER_END"

        start_subtest "Test the 'pipeline_end' condition"
        B2C_HOOK_PIPELINE_END=""
        call_mcli_mirror_on_hook_conditions "pipeline_end" "args"
        call_mcli_mirror_on_hook_conditions "pipeline_end" "args2"
        assertEquals "mc mirror args\nmc mirror args2\n" "$B2C_HOOK_PIPELINE_END"

        start_subtest "Test the 'change' condition"
        B2C_HOOK_PIPELINE_START=""
        call_mcli_mirror_on_hook_conditions "changes" "args"
        call_mcli_mirror_on_hook_conditions "changes" "args2"
        assertEquals "start_daemon_cmd mc mirror --watch args\nqueue_pipeline_end_cmd mc mirror args\nstart_daemon_cmd mc mirror --watch args2\nqueue_pipeline_end_cmd mc mirror args2\n" "$B2C_HOOK_PIPELINE_START"
    }

    run_unit_test test
}
suite_addTest testCall_mcli_mirror_on_hook_conditions


testStart_background_cmd() {
    test() {
        function mycmd {
            /bin/true
        }

        function sleep {
            /bin/true
        }

        B2C_HOOK_PIPELINE_END=""
        start_daemon_cmd "mycmd"

        # Check the killing is queued at the end of the pipeline
        assertEquals "pkill -9 -P $!\n" "$B2C_HOOK_PIPELINE_END"

        # Kill the daemon-restart loop
        execute_hooks "pipeline_end_hook" "$B2C_HOOK_PIPELINE_END"
    }

    run_unit_test test
}
suite_addTest testStart_background_cmd


testQueue_pipeline_end_cmd() {
    test() {
        B2C_HOOK_PIPELINE_END=""
        queue_pipeline_end_cmd "mycmd"

        # Check the killing is queued at the end of the pipeline
        assertEquals "mycmd\n" "$B2C_HOOK_PIPELINE_END"
    }

    run_unit_test test
}
suite_addTest testQueue_pipeline_end_cmd


testSetupVolume__Volume_expiration() {
    test() {
        start_subtest "Check the handling of invalid expirations"
        podman_calls=""
        setup_volume "volume,expiration=invalid"
        assertEquals 1 $?
        assertEquals '' "$podman_calls"

        start_subtest "Check a valid expression"
        podman_calls=""
        PODMAN_EXISTS_EXIT_CODE=1
        setup_volume "volume,expiration=pipeline_end"
        assertEquals 0 $?
        assertEquals 'volume exists volume\nvolume create --label expiration=pipeline_end volume\n' "$podman_calls"
    }

    run_unit_test test
}
suite_addTest testSetupVolume__Volume_expiration


test__remove_expired_volumes() {
    test() {
        start_subtest "Check the expected case"
        podman_calls=""
        PODMAN_LIST_RETURN_STR="volume1\nvolume3\n"
        remove_expired_volumes
        assertEquals 0 $?
        # NOTE: The `podman volume list` call is tested in the podman function because it is run in a subshell
        assertEquals 'volume rm --force volume1\nvolume rm --force volume3\n' "$podman_calls"

        start_subtest "Check error when listing"
        podman_calls=""
        PODMAN_LIST_RETURN_STR=""
        PODMAN_LIST_EXIT_CODE=42
        remove_expired_volumes
        assertEquals 42 $?
        assertEquals '' "$podman_calls"
    }

    run_unit_test test
}
suite_addTest test__remove_expired_volumes


testMountFilesystem() {
    test() {
        mount() {
            local OLDIFS=$IFS IFS=' '
            mount_calls="$mount_calls$@\n"
            IFS=$OLDIFS
        }
        mount_calls=""

        ARG_FILESYSTEM="fs1\nfs2,src=,type=p9\nsimple_fs,src=/dev/test\ncomplete_fs,src=myvirtdrive,type=p9,opts=opt1|opt2\n"

        start_subtest "Basic test"
        mount_calls=""
        mount_filesystem "simple_fs" "/my/path"
        assertEquals 0 $?
        assertEquals '/dev/test /my/path\n' "$mount_calls"

        start_subtest "All options"
        mount_calls=""
        mount_filesystem "complete_fs" "/my/path"
        assertEquals 0 $?
        assertEquals '-t p9 -o opt1,opt2 myvirtdrive /my/path\n' "$mount_calls"

        start_subtest "Incomplete definition - No parameters"
        mount_calls=""
        mount_filesystem "fs1" "/my/path"
        assertEquals 1 $?
        assertEquals '' "$mount_calls"

        start_subtest "Incomplete definition - Empty source parameter"
        mount_calls=""
        mount_filesystem "fs2" "/my/path"
        assertEquals 1 $?
        assertEquals '' "$mount_calls"
    }
    run_unit_test test
}
suite_addTest testMountFilesystem
