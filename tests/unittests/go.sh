source ./tests/unittests/base.sh

run() {
    tmpfile=$(mktemp /tmp/gotest.XXXXXX)

    (set -o pipefail; go test $@ 2>&1 | tee "$tmpfile")
    ret=$?

    output=$(cat "$tmpfile")
    rm "$tmpfile"

    return $ret
}

# Tests
testGoPkgCmdline() {
    test() {
        run -coverprofile=./coverage/cmdline.out ./pkg/cmdline/*.go
        assertEquals 0 $?
        assertContains "$output" "coverage: 100.0% of statements"
    }

    run_unit_test test
}
suite_addTest testGoPkgCmdline

testGoPkgFilesystem() {
    test() {
        run -coverprofile=./coverage/filesystem.out ./pkg/filesystem/*.go
        assertEquals 0 $?
        assertContains "$output" "coverage: 100.0% of statements"
    }

    run_unit_test test
}
suite_addTest testGoPkgFilesystem

testGoPkgCacheDevice() {
    test() {
        run -coverprofile=./coverage/cache_device.out.tmp ./pkg/cache_device/*.go
        assertEquals 0 $?

        # Ignore cache_device_linux.go for code coverage since it cannot be tested in unittests
        grep -v "_linux.go" ./coverage/cache_device.out.tmp > ./coverage/cache_device.out
        coverage=$(go tool cover -func=./coverage/cache_device.out | grep "total:" | xargs)
        echo "Adjusted coverage: $coverage"
        assertContains "$coverage" "total: (statements) 100.0%"
    }

    run_unit_test test
}
suite_addTest testGoPkgCacheDevice

testGoFmt() {
    unformatedfiles="$(gofmt -l .)"

    if [ -n "$unformatedfiles" ]; then
        echo "List of unformated files: $unformatedfiles"
        return 1
    else
        echo "All the files are well formated!"
        return 0
    fi
}
suite_addTest testGoFmt
