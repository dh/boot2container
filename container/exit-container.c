#include <stdio.h>
#include <stdlib.h>

/* How to:
 * $ musl-gcc -s -o exit-container exit-container.c -static -Os
 * $ EXIT_CODE=23 ./exit-container; echo $?
 */
int main(int argc, char **argv)
{
    char *exit_code_s = NULL;
    int exit_code = 0;

    exit_code_s = getenv("EXIT_CODE");
    if (exit_code_s != NULL) {
        exit_code = atoi(exit_code_s);
    }

    printf("Exiting with code: %i\n", exit_code);

    return exit_code;
}
